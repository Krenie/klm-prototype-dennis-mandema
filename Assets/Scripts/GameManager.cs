﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    // list for the numbers of the hangers
    int[] hangerNumber = new int[3]
    {
        1,2,3
    };

    // struct, information over the hanger
    public HangerInfo[] hangerInfo;

    // data of the airplane + navmeshagent + travel points
    public AirplaneData[] airplanes;

    // going through all the hangers and set the TMP to the numbers
    void Start()
    {
        for (int i = 0; i < hangerInfo.Length; i++)
        {
            hangerInfo[i].hangerText.text = hangerNumber[i].ToString();
        }
    }

    public void Park()
    {
        // static variable, going over all scripts
        AirplaneData.Park = !AirplaneData.Park;
        if (AirplaneData.Park)
        {
            // looping through the airplanes
            for (int i = 0; i < airplanes.Length; i++)
            {
                // looping through the hangers
                for (int j = 0; j < hangerInfo.Length; j++)
                {
                    // looking at text of hanger same as text of airplane, then set destination to same number
                    if (hangerInfo[j].hangerText.text == airplanes[i].airplaneNumber.text)
                    {
                        airplanes[i].agent.SetDestination(hangerInfo[j].hangerParkingSpot.transform.position);
                    }
                }
            }
        }
    }

    // toggle switch of the lights
    public void ToggleLight()
    {
        for (int i = 0; i < airplanes.Length; i++)
        {
            for (int j = 0; j < airplanes[i].airplaneLight.Length; j++)
            {
                airplanes[i].airplaneLight[j].enabled = !airplanes[i].airplaneLight[j].enabled;
            }
        }
    }
}
// saving the data of the hangers
[System.Serializable]
public struct HangerInfo
{
    public GameObject hangerParkingSpot;
    public TMP_Text hangerText;
}
