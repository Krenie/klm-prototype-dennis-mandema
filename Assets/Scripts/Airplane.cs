﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Airplane", menuName = "Airplane")]
public class Airplane : ScriptableObject
{
    [Tooltip("Type name of the airplane")]
    public string type;
    [Tooltip("Brand of the airplane")]
    public string brand;
}
