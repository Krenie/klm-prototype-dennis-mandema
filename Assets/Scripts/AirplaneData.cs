﻿
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class AirplaneData : MonoBehaviour
{
    public Airplane airplane;

    public TMP_Text airplaneNumber;

    public NavMeshAgent agent;

    public Light[] airplaneLight;

    public Transform[] points;

    public static bool Park;

    void Start()
    {
        airplaneNumber.text = airplane.type;
        // Getting a random point in the list and then setting it as destination
        agent.SetDestination(points[Random.Range(0, points.Length - 1)].position);
    }

    private void Update()
    {
        // If not parked get a destination
        if (!Park)
        {
            // What is the remaining distance to get to the next point
            float dist = agent.remainingDistance;
            // If agent is at the point, get a new position
            if (dist != Mathf.Infinity && agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance == 0)
            {
                // Getting a random point in the list and then setting it as destination
                agent.SetDestination(points[Random.Range(0, points.Length - 1)].position);
            }
        }
    }
}
